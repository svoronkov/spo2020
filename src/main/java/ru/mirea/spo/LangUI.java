package ru.mirea.spo;

import ru.mirea.spo.exception.LangParseException;
import ru.mirea.spo.lexer.Lexer;
import ru.mirea.spo.parser.Parser;

public class LangUI {

    public static void main(String[] args) throws LangParseException {
        System.out.println("Start lang UI");

        // начать считывать данные из файла, который передан первым аргументом args[1]
        String rawInput = "";

        Lexer lexer = new Lexer(rawInput);

        Parser parser = new Parser( lexer.tokens() );

        parser.lang();

    }

}
