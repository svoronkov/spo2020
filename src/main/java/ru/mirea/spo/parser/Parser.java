package ru.mirea.spo.parser;

import ru.mirea.spo.exception.LangParseException;
import ru.mirea.spo.lexer.LexemType;
import ru.mirea.spo.token.Token;

import java.util.List;

/**
 * Основная цель синтаксического анализатора
 * - проверка синтаксиса
 * (корректности входного текста)
 */
public class Parser {

    private final List<Token> tokens;

    public Parser(List<Token> tokens) {
        this.tokens = tokens;
    }

    public void lang() throws LangParseException {
        while(true) {
            expr();
        }
    }

    private void expr() throws LangParseException {
        var();
        assign_op();
        value_expr();
    }

    private void var() throws LangParseException {
        matchToken(match(), LexemType.VAR);
    }

    private Token match() {
        // возвращает следующий токен
        return null;
    }

    private void assign_op() throws LangParseException {
        matchToken(match(), LexemType.ASSIGN_OP);
    }

    private void value_expr() throws LangParseException {
        value();
        //TODO
        while (true) {
            op();
            value();
        }

    }

    private void value() throws LangParseException {
        try {
            var();
        } catch (LangParseException e){
            digit();
        }
    }

    private void digit() throws LangParseException {
        matchToken(match(), LexemType.DIGIT);
    }

    private void op() throws LangParseException {
        matchToken(match(), LexemType.OP);
    }

    private void matchToken(Token token, LexemType type) throws LangParseException {
        if (!token.getType().equals(type)) {
            throw new LangParseException(type
                    + " expected but "
                    + token.getType().name() + ": " + token.getValue()
                    + " found");
        }
    }

}
