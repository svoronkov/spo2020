package ru.mirea.spo.lexer;

import java.util.regex.Pattern;

public enum LexemType {

    VAR("[a-z]"),
    DIGIT("0|([1-9][0-9]*)"),
    ASSIGN_OP("="),
    OP("+|-|*|/");

    private Pattern pattern;

    LexemType(String regexp) {
        this.pattern = Pattern.compile(regexp);
    }

}
