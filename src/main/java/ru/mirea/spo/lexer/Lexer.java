package ru.mirea.spo.lexer;

import ru.mirea.spo.token.Token;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {

    private final String rawInput;

    public Lexer(String rawInput) {

        this.rawInput = rawInput;
        System.out.println("Create lexer with input string: " + this.rawInput);
    }

    public List<Token> tokens() {
        return Collections.emptyList();
    }

    public static void main(String[] args) {

        String input = "catsAge";

        Pattern pattern = Pattern.compile("^[a-zA-Z]+$");

        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            System.out.println("found");
        } else {
            System.out.println("not found");
        }

    }

}
