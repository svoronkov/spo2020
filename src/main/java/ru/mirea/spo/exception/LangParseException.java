package ru.mirea.spo.exception;

public class LangParseException extends Exception {
    public LangParseException(String s) {
        super(s);
    }
}
